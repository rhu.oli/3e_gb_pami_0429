import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'io.ionic.starter',
  appName: 'AppNavegacaoOO',
  webDir: 'www',
  bundledWebRuntime: false
};

export default config;
